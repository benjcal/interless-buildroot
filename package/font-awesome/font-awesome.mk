################################################################################
#
# font-awesome
#
################################################################################

FONT_AWESOME_VERSION = v4.7.0
FONT_AWESOME_SITE = $(call github,FortAwesome,Font-Awesome,$(FONT_AWESOME_VERSION))
FONT_AWESOME_LICENSE = OFL-1.1 (font), MIT (CSS, LESS and Sass files)

define FONT_AWESOME_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/share/fonts/font-awesome/
	cp $(@D)/fonts/* $(TARGET_DIR)/usr/share/fonts/font-awesome/
endef

$(eval $(generic-package))
