################################################################################
#
# interless-qt
#
################################################################################

INTERLESS_QT_VERSION = v0.4
INTERLESS_QT_SITE = git@gitlab.com:benjcal/interless-qt.git
INTERLESS_QT_SITE_METHOD = git
INTERLESS_QT_DEPENDENCIES = qt5multimedia qt5quickcontrols2 qt5graphicaleffects

define INTERLESS_QT_CONFIGURE_CMDS
	(cd $(@D); $(TARGET_MAKE_ENV) $(QT5_QMAKE) PREFIX=/usr)
endef

define INTERLESS_QT_BUILD_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D)
endef

define INTERLESS_QT_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0755 $(@D)/interless-qt $(TARGET_DIR)/usr/bin
endef

$(eval $(generic-package))
